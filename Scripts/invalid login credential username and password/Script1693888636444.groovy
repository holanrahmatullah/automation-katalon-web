import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

def testData = TestDataFactory.findTestData('Data Files/invalidlogin') // Ganti dengan path file CSV Anda

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/login')
for (def row = 1; row <=  testData.getRowNumbers(); row++) {
	WebUI.verifyElementText(findTestObject('Login/Page_Masuk untuk dapatkan akses di Coding.ID/label_Email'), 'Email')
	
	WebUI.setText(findTestObject('Object Repository/Login/Page_Masuk untuk dapatkan akses di Coding.ID/input_Email_email'),
		findTestData('invalidlogin').getValue(1, row))
	
	WebUI.verifyElementText(findTestObject('Login/Page_Masuk untuk dapatkan akses di Coding.ID/label_Kata                                                            Sandi'),
		'Kata Sandi')
	
	WebUI.setEncryptedText(findTestObject('Object Repository/Login/Page_Masuk untuk dapatkan akses di Coding.ID/input_Kata                                 _98da12'),
		findTestData('invalidlogin').getValue(2, row))
	
	WebUI.verifyElementClickable(findTestObject('Login/Page_Masuk untuk dapatkan akses di Coding.ID/button_Login'))
	
	WebUI.verifyElementText(findTestObject('Login/Page_Masuk untuk dapatkan akses di Coding.ID/button_Login'), 'Login')
	
	WebUI.click(findTestObject('Object Repository/Login/Page_Masuk untuk dapatkan akses di Coding.ID/button_Login'))
	
	WebUI.verifyElementText(findTestObject('Login/Page_Masuk untuk dapatkan akses di Coding.ID/small_Email atau kata sandi salah'),
		'Email atau kata sandi salah')
	
}




WebUI.closeBrowser()

