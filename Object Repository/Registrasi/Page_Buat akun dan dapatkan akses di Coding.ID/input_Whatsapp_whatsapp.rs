<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Whatsapp_whatsapp</name>
   <tag></tag>
   <elementGuidId>6c60e7a2-cda2-41d8-b0ca-da18c9ca4f01</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='whatsapp']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#whatsapp</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ae63fc69-4982-4dc2-a7ad-7d48b618eae3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>whatsapp</value>
      <webElementGuid>9af6a12d-95b2-400d-bf87-33a9841da6b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>number</value>
      <webElementGuid>77a11793-93e4-4d25-b1f0-7ec36de9d11c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>WhatsApp </value>
      <webElementGuid>06320b56-46b0-4b79-95c3-52ce9ec26421</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control </value>
      <webElementGuid>9bddc941-f180-4097-a3ef-495e0296f68f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>whatsapp</value>
      <webElementGuid>0ea62654-9d7a-40f9-8789-21f3dcd65538</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>whatsapp</value>
      <webElementGuid>c6152b22-5579-41ea-a1ec-c5a3fdb419f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;whatsapp&quot;)</value>
      <webElementGuid>5f959ace-5cf6-4f1f-9702-269557cfc030</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='whatsapp']</value>
      <webElementGuid>0fe1b060-f8fa-4473-8722-03191179e09e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/input</value>
      <webElementGuid>e6a37728-6a00-4c0b-aa04-ae7bdf8227d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'whatsapp' and @type = 'number' and @placeholder = 'WhatsApp ' and @name = 'whatsapp']</value>
      <webElementGuid>5d94d5c2-1df8-492c-8cd0-4cbb6ecf0dba</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
