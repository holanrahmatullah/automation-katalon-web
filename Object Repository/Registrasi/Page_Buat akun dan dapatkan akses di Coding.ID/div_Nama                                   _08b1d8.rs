<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Nama                                   _08b1d8</name>
   <tag></tag>
   <elementGuidId>7530889c-d06f-4a91-ac4c-14c9cd1aa453</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat Akun Baru'])[1]/following::div[5]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.card-body > div.container > div.row</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e5708f7d-86b1-4b40-991c-82e69739898c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row</value>
      <webElementGuid>0e284258-4cd1-4aab-aa94-1110bd8dc998</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        

                            
                                
                                
                                    Nama

                                    
                                        

                                                                            
                                
                                
                                    Tanggal lahir

                                    
                                        

                                                                            
                                


                                
                                    E-Mail

                                    
                                        

                                                                            
                                
                                
                                    Whatsapp

                                    
                                        

                                                                            
                                

                                
                                    Kata Sandi

                                    
                                        
                                                                            
                                

                                
                                    Konfirmasi kata sandi


                                    
                                        
                                                                            
                                
                                
                                    
                                        
                                        
                                            
                                                Saya setuju
                                                    dengan syarat dan ketentuan yang
                                                    berlaku
                                            
                                        
                                    
                                

                                
                                    
                                        
                                            Daftar
                                        
                                    
                                
                                
                                
                                     
                                            
                                            Sign-in With Google
                                        
                                

                                
                                    Sudah punya akun ?Silahkan
                                        Masuk
                                
                            
                        

                    </value>
      <webElementGuid>2d4e39f6-5101-410b-a7ff-c14061a8a98d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents hiddenscroll mathml unicoderange no-touchevents unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[1]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-md-12 col-sm-12 col-xs-12&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]</value>
      <webElementGuid>7f5727d2-8aaa-4c54-acf3-14d0b24d9bd2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat Akun Baru'])[1]/following::div[5]</value>
      <webElementGuid>7fb92e44-42ad-4cf4-9f3a-10fd7ae457e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/div/div/div/div</value>
      <webElementGuid>893d34b9-eef1-44b0-a3de-de20fd95cd6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        

                            
                                
                                
                                    Nama

                                    
                                        

                                                                            
                                
                                
                                    Tanggal lahir

                                    
                                        

                                                                            
                                


                                
                                    E-Mail

                                    
                                        

                                                                            
                                
                                
                                    Whatsapp

                                    
                                        

                                                                            
                                

                                
                                    Kata Sandi

                                    
                                        
                                                                            
                                

                                
                                    Konfirmasi kata sandi


                                    
                                        
                                                                            
                                
                                
                                    
                                        
                                        
                                            
                                                Saya setuju
                                                    dengan syarat dan ketentuan yang
                                                    berlaku
                                            
                                        
                                    
                                

                                
                                    
                                        
                                            Daftar
                                        
                                    
                                
                                
                                
                                     
                                            
                                            Sign-in With Google
                                        
                                

                                
                                    Sudah punya akun ?Silahkan
                                        Masuk
                                
                            
                        

                    ' or . = '
                        

                            
                                
                                
                                    Nama

                                    
                                        

                                                                            
                                
                                
                                    Tanggal lahir

                                    
                                        

                                                                            
                                


                                
                                    E-Mail

                                    
                                        

                                                                            
                                
                                
                                    Whatsapp

                                    
                                        

                                                                            
                                

                                
                                    Kata Sandi

                                    
                                        
                                                                            
                                

                                
                                    Konfirmasi kata sandi


                                    
                                        
                                                                            
                                
                                
                                    
                                        
                                        
                                            
                                                Saya setuju
                                                    dengan syarat dan ketentuan yang
                                                    berlaku
                                            
                                        
                                    
                                

                                
                                    
                                        
                                            Daftar
                                        
                                    
                                
                                
                                
                                     
                                            
                                            Sign-in With Google
                                        
                                

                                
                                    Sudah punya akun ?Silahkan
                                        Masuk
                                
                            
                        

                    ')]</value>
      <webElementGuid>352a65d1-f485-4dac-9e20-68c66c885abc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
